<?php
namespace App\Exam;

class Exam
{
    public $id;
    public $name;
    public $phone;
    public $email;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","labexamayman") or die("DB Failed");
    }
    public function prepare($data="")
    {
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if(array_key_exists('phone',$data))
        {
            $this->phone=$data['phone'];
        }
        if(array_key_exists('email',$data))
        {
            $this->email=$data['email'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `labexamayman`.`user` (`name`, `phone`, `email`) VALUES ('".$this->name."','".$this->phone."','".$this->email."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Yes";
        }else
        {
            echo "No!";
        }
        Utility::redirect('index.php');
    }

    public function index()
    {
        $_allUser=array();
        $query="SELECT * FROM `labexamayman`.`user`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }

    public function count()
    {
        $query="SELECT COUNT(*) AS totalitem FROM `labexamayman`.`user`";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row['totalitem'];
    }
    public function paginate($PageStartFrom=0,$limit=5)
    {
        $_allUser=array();
        $query="SELECT * FROM `labexamayman`.`user` LIMIT ".$PageStartFrom.",".$limit;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_allUser[]=$row;
        }
        return $_allUser;
    }
    public function view()
    {
        $query="SELECT * FROM `labexamayman`.`user` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {
        $query="UPDATE `labexamayman`.`user` SET `name`='".$this->name."',`phone`='".$this->phone."',`email`='".$this->email."' WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Yes";
        }else
        {
            echo "No!";
        }
        Utility::redirect('index.php');
    }

    public function delete()
    {
        $query="DELETE FROM `labexamayman`.`user` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Yes";
        }else
        {
            echo "No!";
        }
        Utility::redirect('index.php');
    }
}