<?php
include_once('../../vendor/autoload.php');

use App\Exam\Exam;
use App\Exam\Utility;


$user=new Exam();
$user->prepare($_GET);
$singleItem=$user->view();

?>
<!Doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <center><h2>Update User</h2></center>
    <br>
    <form role="form" action="update.php" method="post" class="form-group">
        <input type="hidden" name="id" class="form-control" value="<?php echo $singleItem['id'] ?>">
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" value="<?php echo $singleItem['name'] ?>">
        </div>
        <div class="form-group">
            <label>Phone No:</label>
            <input type="text" name="phone" class="form-control" value="<?php echo $singleItem['phone'] ?>">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="<?php echo $singleItem['email'] ?>">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
</body>
</html>
