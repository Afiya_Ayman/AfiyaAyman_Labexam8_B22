<?php

session_start();
include_once('../../vendor/autoload.php');

use App\Exam\Exam;
use App\Exam\Utility;
use App\Exam\Message;


$user=new Exam();

    if(array_key_exists('itemPerPage',$_SESSION))
    {
        if(array_key_exists('itemPerPage',$_GET))
        {
            $_SESSION['itemPerPage']=$_GET['itemPerPage'];
        }
    }else{
        $_SESSION['itemPerPage']=5;
    }

    $itemPerPage=$_SESSION['itemPerPage'];
    $totalItem=$user->count();
    //Utility::dd($totalItem);
    $totalPage=ceil($totalItem/$itemPerPage);

    if(array_key_exists('pageNumber',$_GET))
    {
        $pageNumber=$_GET['pageNumber'];
    }
    else
    {
        $pageNumber=1;
    }

    $pagination="";
    for($i=1;$i<=$totalPage;$i++)
    {
        $class=($pageNumber==$i)?"active":"";
        $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
    }

    $PageStartFrom=$itemPerPage*($pageNumber-1);
    $prevPage=$pageNumber-1;
    $nextPage=$pageNumber+1;
    $allUser=$user->paginate($PageStartFrom,$itemPerPage);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
</head>
<body>

<div class="container">
    <center><h2>All User List</h2></center>
    <br><br>
    <a href="create.php" class="btn btn-primary" role="button">Insert Again</a>
    <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
    <br>
    <div id="message">
        <?php echo Message::message(); ?></div>
    <br>
    <form role="form">
        <div class="form-group">
            <label>Item Per page?</label>
            <select class="form-control" name="itemPerPage">
                <option<?php if($itemPerPage==5){?> selected <?php }?>>5</option>
                <option<?php if($itemPerPage==10){?> selected <?php }?>>10</option>
                <option<?php if($itemPerPage==15){?> selected <?php }?>>15</option>
            </select>
            <br>
            <button type="submit" class="btn btn-info">Go!</button>
        </div>
    </form>
    <br>
    <br><br>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allUser as $info){
                $sl++;
                ?>
                <td><?php echo $sl+$PageStartFrom?></td>
                <td><?php echo $info['id']?></td>
                <td><?php echo $info['name']?></td>
                <td><?php echo $info['phone']?></td>
                <td><?php echo $info['email']?></td>
                <td><a href="view.php?id=<?php echo $info['id'] ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info['id']?>" class="btn btn-success" role="button">Edit</a>
                    <a href="mail.php?id=<?php echo $info['id']?>" class="btn btn-warning" role="button">Mail User</a>
                    <a href="delete.php?id=<?php echo $info['id']?>" class="btn btn-danger" role="button">Delete</a>
                </td>


            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
        <center><ul class="pagination">
                <?php if($pageNumber>1){?>
                    <li><a href="index.php?pageNumber=<?php echo $prevPage?>">Prev</a></li>
                <?php }?>
                <?php echo $pagination?>
                <?php if($pageNumber<$totalPage){?>
                    <li><a href="index.php?pageNumber=<?php echo $nextPage?>">Next</a></li>
                <?php }?>
            </ul></center>

</div>


<script>
    $('#message').show().delay(2000).fadeOut()
</script>

</body>
</html>

