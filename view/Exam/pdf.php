<?php

require_once('../../vendor/mpdf/mpdf/mpdf.php');

include_once('../../vendor/autoload.php');

use App\Exam\Exam;
use App\Exam\Utility;


$user=new Exam();
$allUser=$user->index();
$string="";
$sl=0;
foreach($allUser as $user):
    $sl++;
    $string.="<tr>";
    $string.="<td>$sl</td>";
    $string.="<td>".$user['id']."</td>";
    $string.="<td>".$user['name']."</td>";
    $string.="<td>".$user['phone']."</td>";
    $string.="<td>".$user['email']."</td>";
    $string.="</tr>";
    endforeach;

$html=<<<EOD
<table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>

            </tr>
            </thead>
            <tbody>
                $string
            </tbody>
        </table>
EOD;



$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output();